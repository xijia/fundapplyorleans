﻿using System;
using System.Threading.Tasks;

using System.Windows.Forms;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using GrainInterfaces;



namespace ReviewerClient
{
	public partial class ShowApplicants : Form
	{
		private int applicantIndex = 21;//找个方法直接返回第一个 简单的方法
        private int reviewerIndex = 15;
        private int phase = 0;
        private int title = 0;
        private IClusterClient client;
        private string _reveiewerName;

        private Guid guid;
        private IReviewerEvents reviewerEvents;
        private ICommonTools commonTools;

        public ShowApplicants(int reviewIndex, IClusterClient client,
                                string reviewerName)
		{
            this.client = client;
            _reveiewerName = reviewerName;

            guid = Guid.NewGuid();
            reviewerEvents = client.GetGrain<IReviewerEvents>(guid);
            commonTools = client.GetGrain<ICommonTools>(0);

            InitializeComponent();
            reviewerIndex = reviewIndex;
            GetReviewInfo();
            ShowApplicantByIndex();

        }

		public async void ShowApplicantByIndex() 
		{    
            
            //var commonTools = client.GetGrain<ICommonTools>(0);
            var info =await commonTools.FindApplicant(applicantIndex,_reveiewerName);
            var updateStates = await commonTools.UpdateState(phase, applicantIndex);
            if(!updateStates)
            {
                MessageBox.Show($"update state wrong please contact it", "fail",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                applicantIndex = 21;
            }
            if (info == null)
            {
                MessageBox.Show($"cannot move forward or backword", "fail",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                applicantIndex = 21;
            }
            else
            {
                txtName.Text = info.Name;
                txtFund.Text = info.Fund.ToString();
                richReason.Text = info.Reason;
                txtState.Text = info.States;
                if (info.accountNum.Length > 2)
                    txtAccountNumber.Text = info.accountNum;
                else
                    txtAccountNumber.Text = "not submitteds";
                txtANState.Text = info.AccountNumState;
            }

        }


        private void btnPre_Click(object sender, EventArgs e)
        {
            applicantIndex--;
            ShowApplicantByIndex();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            //if()
            applicantIndex++;
            ShowApplicantByIndex();
        }

        private async void btnApprove_Click(object sender, EventArgs e)
        {
            if(reviewerIndex == 11)//在这里改一下 hardcore了 accountant
            {
                //var acc = client.GetGrain<IAccountNum>(0);
                //bool response = await acc.ChangeAccountNumState(txtAccountNumber.Text,
                //                                                applicantIndex, 3);

                var response = await reviewerEvents.AccountantApprove(txtAccountNumber.Text, 
                                                                      applicantIndex);
                if (response)
                {
                    MessageBox.Show($"Subitted!", "Success",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show($"cannot submit please cotact us!", "fail",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                //var commonTools = client.GetGrain<ICommonTools>(0);
                //var update = await commonTools.UpdateState(phase + 1, applicantIndex);

                bool update = false;
                switch (_reveiewerName)
                {
                    case "reviewer1":
                        update = await reviewerEvents.Reviewer1Appove(applicantIndex);
                        break;
                    case "Lawyer":
                        update = await reviewerEvents.LawyerReviewApprove(applicantIndex);
                        break;
                    case "reviewer3":
                        update = await reviewerEvents.Reviewer3Approve(applicantIndex);
                        break;
                    case "reviewer4":
                        update = await reviewerEvents.Reviewer4Approve(applicantIndex);
                        break;
                    case "Architect":
                        update = await reviewerEvents.ArchitectReviewApprove(applicantIndex);
                        break;
                    case "BoardMeeting":
                        update = await reviewerEvents.BoardMeetingApprove(applicantIndex);
                        break;
                    default:
                        break;
                }



                if (update)
                {
                    MessageBox.Show($"updated the state", "success",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show($"cannot update the state", "fail",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }

        }


        private async void GetReviewInfo() 
        {
            var commonTools = client.GetGrain<ICommonTools>(0);
            (title,phase) = await commonTools.FindReview(reviewerIndex);

            if ((title, phase) == (0,0))
            {
                MessageBox.Show($"wrong user name or password", "fail",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                applicantIndex = 12;
            }

        }

        private async void btnReject_Click(object sender, EventArgs e)
        {
            if (reviewerIndex == 11)//在这里改一下 hardcore了 accountant
            {
                //var acc = client.GetGrain<IAccountNum>(0);
                //bool response = await acc.ChangeAccountNumState(txtAccountNumber.Text,
                //                                                applicantIndex, 4);
                bool response = await reviewerEvents.AccountantReject(txtAccountNumber.Text, applicantIndex);

                if (response)
                {
                    MessageBox.Show($"Subitted!", "Success",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show($"cannot submit please cotact us!", "fail",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                //var commonTools = client.GetGrain<ICommonTools>(0);
                //var update = await commonTools.UpdateState(6, applicantIndex);

                bool update = false;
                switch (_reveiewerName)
                {
                    case "reviewer1":
                        update = await reviewerEvents.Reviewer1Reject(applicantIndex);
                        break;
                    case "Lawyer":
                        update = await reviewerEvents.LawyerReviewReject(applicantIndex);
                        break;
                    case "reviewer3":
                        update = await reviewerEvents.Reviewer3Reject(applicantIndex);
                        break;
                    case "reviewer4":
                        update = await reviewerEvents.Reviewer4Reject(applicantIndex);
                        break;
                    case "Architect":
                        update = await reviewerEvents.ArchitectReviewReject(applicantIndex);
                        break;
                    case "BoardMeeting":
                        update = await reviewerEvents.BoardMeetingReject(applicantIndex);
                        break;
                    default:
                        break;
                }

                if (update)
                {
                    MessageBox.Show($"updated the state", "success",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show($"cannot update the state", "fail",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
                
        }

        private async void btnComplete_Click(object sender, EventArgs e)
        {
            //var account = client.GetGrain<IAccountNum>(0);
            //var update = await account.ChangeCompleteState(applicantIndex,1);
            var update = await reviewerEvents.CompleteState(applicantIndex);

            if (update)
            {
                MessageBox.Show($"updated the state", "success",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show($"cannot update the state", "fail",
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
