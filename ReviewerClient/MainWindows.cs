﻿using Orleans;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReviewerClient
{
	public partial class MainWindows : Form
	{

		public MainWindows(IClusterClient client)
		{
			InitializeComponent();
			ShowLoggin(client);
		}

		void ShowLoggin(IClusterClient client)
		{
			var reviewerLoggin = new ReviewerLogin(client);
			reviewerLoggin.MdiParent = this;
			reviewerLoggin.Show();
		}
	}
}
