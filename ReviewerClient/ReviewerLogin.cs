﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Runtime;
using GrainInterfaces;

namespace ReviewerClient
{
	public partial class ReviewerLogin : Form
	{
		private int reviewIndex;
		private IClusterClient client;
		public ReviewerLogin(IClusterClient client)
		{
			this.client = client;
			InitializeComponent();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			txtName.Text = "";
			txtPwd.Text = "";
		}

		private async void btnLog_Click(object sender, EventArgs e)
		{

			var appl = client.GetGrain<ICommonTools>(0);
			var n = await appl.CheckLoggin(txtName.Text, txtPwd.Text,false);
					
			switch (n)
			{
				case null:
					MessageBox.Show($"user name or password does not exist", "fail",
					MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					break;
				//case -1:
				//    MessageBox.Show($"bug in sql", "fail",
				//    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				//    break;
				default:
					MessageBox.Show($"Success Log in!", "Success",
					MessageBoxButtons.OK, MessageBoxIcon.Information);
					reviewIndex = (int)n;
					
					var reviewer = new ShowApplicants(reviewIndex, client, txtName.Text);
					this.Close();
					reviewer.MdiParent = this.MdiParent;
					reviewer.Show();
					break;
			}


		}

		
	}
}
