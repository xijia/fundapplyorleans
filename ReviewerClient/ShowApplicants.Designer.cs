﻿namespace ReviewerClient
{
	partial class ShowApplicants
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.txtName = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.txtFund = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.richReason = new System.Windows.Forms.RichTextBox();
			this.btnPre = new System.Windows.Forms.Button();
			this.btnNext = new System.Windows.Forms.Button();
			this.btnApprove = new System.Windows.Forms.Button();
			this.btnReject = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.txtState = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.txtANState = new System.Windows.Forms.Label();
			this.btnComplete = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.txtAccountNumber = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(62, 45);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(29, 12);
			this.label1.TabIndex = 0;
			this.label1.Text = "Name";
			// 
			// txtName
			// 
			this.txtName.AutoSize = true;
			this.txtName.Location = new System.Drawing.Point(134, 45);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(0, 12);
			this.txtName.TabIndex = 1;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(62, 84);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(29, 12);
			this.label3.TabIndex = 2;
			this.label3.Text = "Fund";
			// 
			// txtFund
			// 
			this.txtFund.AutoSize = true;
			this.txtFund.Location = new System.Drawing.Point(136, 84);
			this.txtFund.Name = "txtFund";
			this.txtFund.Size = new System.Drawing.Size(0, 12);
			this.txtFund.TabIndex = 3;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(64, 130);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(41, 12);
			this.label5.TabIndex = 4;
			this.label5.Text = "Reason";
			// 
			// richReason
			// 
			this.richReason.Location = new System.Drawing.Point(136, 130);
			this.richReason.Name = "richReason";
			this.richReason.Size = new System.Drawing.Size(186, 119);
			this.richReason.TabIndex = 5;
			this.richReason.Text = "";
			// 
			// btnPre
			// 
			this.btnPre.Location = new System.Drawing.Point(66, 276);
			this.btnPre.Name = "btnPre";
			this.btnPre.Size = new System.Drawing.Size(75, 23);
			this.btnPre.TabIndex = 6;
			this.btnPre.Text = "Previous";
			this.btnPre.UseVisualStyleBackColor = true;
			this.btnPre.Click += new System.EventHandler(this.btnPre_Click);
			// 
			// btnNext
			// 
			this.btnNext.Location = new System.Drawing.Point(204, 276);
			this.btnNext.Name = "btnNext";
			this.btnNext.Size = new System.Drawing.Size(75, 23);
			this.btnNext.TabIndex = 7;
			this.btnNext.Text = "Next";
			this.btnNext.UseVisualStyleBackColor = true;
			this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
			// 
			// btnApprove
			// 
			this.btnApprove.Location = new System.Drawing.Point(352, 276);
			this.btnApprove.Name = "btnApprove";
			this.btnApprove.Size = new System.Drawing.Size(75, 23);
			this.btnApprove.TabIndex = 8;
			this.btnApprove.Text = "Approve";
			this.btnApprove.UseVisualStyleBackColor = true;
			this.btnApprove.Click += new System.EventHandler(this.btnApprove_Click);
			// 
			// btnReject
			// 
			this.btnReject.Location = new System.Drawing.Point(480, 276);
			this.btnReject.Name = "btnReject";
			this.btnReject.Size = new System.Drawing.Size(75, 23);
			this.btnReject.TabIndex = 9;
			this.btnReject.Text = "Reject";
			this.btnReject.UseVisualStyleBackColor = true;
			this.btnReject.Click += new System.EventHandler(this.btnReject_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(371, 68);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(35, 12);
			this.label2.TabIndex = 10;
			this.label2.Text = "State";
			// 
			// txtState
			// 
			this.txtState.AutoSize = true;
			this.txtState.Location = new System.Drawing.Point(430, 67);
			this.txtState.Name = "txtState";
			this.txtState.Size = new System.Drawing.Size(0, 12);
			this.txtState.TabIndex = 11;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(371, 153);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(125, 12);
			this.label4.TabIndex = 12;
			this.label4.Text = "Account Number State";
			// 
			// txtANState
			// 
			this.txtANState.AutoSize = true;
			this.txtANState.Location = new System.Drawing.Point(371, 174);
			this.txtANState.Name = "txtANState";
			this.txtANState.Size = new System.Drawing.Size(0, 12);
			this.txtANState.TabIndex = 13;
			// 
			// btnComplete
			// 
			this.btnComplete.Location = new System.Drawing.Point(410, 198);
			this.btnComplete.Name = "btnComplete";
			this.btnComplete.Size = new System.Drawing.Size(75, 23);
			this.btnComplete.TabIndex = 14;
			this.btnComplete.Text = "Complete";
			this.btnComplete.UseVisualStyleBackColor = true;
			this.btnComplete.Click += new System.EventHandler(this.btnComplete_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(373, 107);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(89, 12);
			this.label6.TabIndex = 15;
			this.label6.Text = "Account Number";
			// 
			// txtAccountNumber
			// 
			this.txtAccountNumber.AutoSize = true;
			this.txtAccountNumber.Location = new System.Drawing.Point(373, 131);
			this.txtAccountNumber.Name = "txtAccountNumber";
			this.txtAccountNumber.Size = new System.Drawing.Size(0, 12);
			this.txtAccountNumber.TabIndex = 16;
			// 
			// ShowApplicants
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(664, 329);
			this.Controls.Add(this.txtAccountNumber);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.btnComplete);
			this.Controls.Add(this.txtANState);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.txtState);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.btnReject);
			this.Controls.Add(this.btnApprove);
			this.Controls.Add(this.btnNext);
			this.Controls.Add(this.btnPre);
			this.Controls.Add(this.richReason);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.txtFund);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.txtName);
			this.Controls.Add(this.label1);
			this.Name = "ShowApplicants";
			this.Text = "ShowApplicants";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label txtName;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label txtFund;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.RichTextBox richReason;
		private System.Windows.Forms.Button btnPre;
		private System.Windows.Forms.Button btnNext;
		private System.Windows.Forms.Button btnApprove;
		private System.Windows.Forms.Button btnReject;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label txtState;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label txtANState;
		private System.Windows.Forms.Button btnComplete;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label txtAccountNumber;
	}
}