﻿using Orleans.Configuration;
using Orleans.Hosting;
using System;
using System.Net;
using System.Threading.Tasks;
using Grains;
using Orleans;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Orleans.Runtime;
using GrainInterfaces;

namespace SiloHost
{
    class Silo
    {
        public static int Main(string[] args)
        {
            return RunMainAsync().Result;
        }

        private static async Task<int> RunMainAsync()
        {
            try
            {
                var host = await StartSilo();
                Console.WriteLine("Silo started!");
                Console.WriteLine("Press Enter to terminate...");
                Console.ReadLine();

                await host.StopAsync();

                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return 1;
            }
        }

        private static async Task<ISiloHost> StartSilo()
        {
            // define the cluster configuration
            var builder = new SiloHostBuilder()
                .UseLocalhostClustering()
                .Configure<ClusterOptions>(options =>
                {
                    options.ClusterId = "dev";
                    options.ServiceId = "FundApply";
                })
                .Configure<EndpointOptions>(options => options.AdvertisedIPAddress = IPAddress.Loopback)
                
                .AddStateStorageBasedLogConsistencyProvider("StateStorage")
                .AddAdoNetGrainStorageAsDefault(option =>
                {
                    //could be saved in a json file as a configration file
                    option.Invariant = "System.Data.SqlClient";
                    option.ConnectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=Applications;
							Integrated Security=True;Pooling=False";
                    option.UseJsonFormat = true;
                }
                )
                 .AddSimpleMessageStreamProvider("ForTest")//
                 
                 //.AddOutgoingGrainCallFilter<CommonTools>()
                 //    .ConfigureServices(
                 //services => services.AddSingleton<IIncomingGrainCallFilter, LoggingCallFilter>())
                 //.ConfigureApplicationParts(parts => parts.AddApplicationPart(typeof(FirstReview).Assembly).WithReferences())
                 //.ConfigureApplicationParts(parts => parts.AddApplicationPart(typeof(FillOutApplication).Assembly).WithReferences())
                
                .ConfigureLogging(logging => logging.AddConsole());

            builder.AddIncomingGrainCallFilter<InCommingCallFilter>();
            var host = builder.Build();
            await host.StartAsync();
            return host;
        }
    }
}
