﻿using System;
using System.Threading.Tasks;
using GrainInterfaces;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;

namespace Client
{
    class Program
    {
        static int Main(string[] args)
        {
            return RunMainAsync().GetAwaiter().GetResult();
        }

        static async Task<int> RunMainAsync()
        {
            try
            {
                using (var client = await StartClient())
                {
                    var grain = client.GetGrain<IEventSourcingTests>(0);
                    await grain.SendGreetings("Hello");

                    var grain1 = client.GetGrain<IEventSourcingTests>(0);
                    await grain1.SendGreetings("Morning");

                    var grain2 = client.GetGrain<IEventSourcingTests>(0);
                    await grain2.SendGreetings("FeiShu");

                    /*                    var grain = client.GetGrain<IHelloGrain>(0);
                                        var grain1 = client.GetGrain<IHelloGrain>(1);

                                        var rsp = await grain.SayHello("Banana");
                                        var rsp1 = await grain1.SayHello("Yuna");*/



                    Console.WriteLine($"Client is initialized: {client.IsInitialized}");
                }

                return 0;
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
                return -1;
            }
        }

        static async Task<IClusterClient> StartClient()
        {
            var client = new ClientBuilder()

                // Clustering information 
                .Configure<ClusterOptions>(options =>
                {

                    options.ClusterId = "dev";
                    options.ServiceId = "FundApply";

                })

                 // Clustering provider
                 .UseLocalhostClustering()
                 .Build();

            await client.Connect();
            Console.WriteLine("Client connected!");

            return client;
        }
    }
}

