﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GrainInterfaces;
using Orleans;
namespace Grains
{
	class smallExample : Orleans.Grain, IsmallExample
	{
/*		public override Task OnActivateAsync()
		{
			log.LogInformation("in OnActivateAsync");
			var streamProvider = GetStreamProvider("ForTest");

			stream = streamProvider.GetStream<EventMsg>(myGuid,
											this.GetPrimaryKeyString());
			return base.OnActivateAsync();
		}*/
		public Task FillOutAnExpenseReport()
		{
			/*			var eventGrain = GrainFactory.GetGrain<IEventProcess>("first");

						var streamId = await eventGrain.GetGuid();
			*/
			var streamProvider = GetStreamProvider("ForTest");
			return Task.CompletedTask;
		}

		public Task WithdrawTheExpenseReport() 
		{
			/*			var test = GrainFactory.GetGrain<ICommonTools>(0);
						test.ForTest();*/
			return Task.CompletedTask;
		}

		public Task Approve()
		{
			return Task.CompletedTask;
		}
		public Task Reject()
		{
			return Task.CompletedTask;
		}
		public Task Payout()
		{
			return Task.CompletedTask;
		}

	}
}
