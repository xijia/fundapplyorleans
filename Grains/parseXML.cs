﻿/*using GrainInterfaces;
using System;
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;

namespace Grains
{
    class parseXML
    {
        public static (HashSet<string> , EventProcess) parseXMLTester(string file)
        {
            //@"C:\orleans\FundApplyOrleans\With_Constrains.xml"
            XmlDocument xmlDoc = new XmlDocument();
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreComments = true;
            XmlReader reader = XmlReader.Create(file, settings);
            xmlDoc.Load(reader);
            var allGroups = new Dictionary<string, List<string>>();//key is the parent
            HashSet<Relation> relations = new HashSet<Relation>();
            //HashSet<string> includedFunc = new HashSet<string>();
            //var process = new EventProcess();
            //acquire the group/nesting
            foreach (XmlElement events in xmlDoc.SelectNodes(@"dcrgraph/specification/resources/events"))
            {
                getEvents(events, "root", allGroups);
            }
            //included
            foreach (XmlElement eventsInclude in xmlDoc.SelectNodes(@"dcrgraph/runtime/marking/included"))
            {
                foreach (XmlElement eventid in eventsInclude)
                {
                    //includedFunc.Add(eventid.GetAttribute("id"));
                    var id = eventid.GetAttribute("id");
                    if (allGroups.ContainsKey(id))
                        continue;
                    process.addEvent(id);
                }
            }
            //constrains
            foreach (XmlElement constraints in xmlDoc.SelectNodes(@"dcrgraph/specification/constraints"))//resources/events/event"))
            {
                foreach (XmlElement conditions in constraints.ChildNodes)
                {
                    foreach (XmlElement cdt in conditions.ChildNodes)
                    {

                        relations.Add(findRelations(cdt.GetAttribute("sourceId"),
                                      conditions.Name, cdt.GetAttribute("targetId")));
                    }
                }
            }

            reader.Close();

            initialEventProcess(process, relations, allGroups);
            var allNames = allFuncNames(allGroups);

            return (allNames, process);
        }

        private static Relation findRelations(string src, string relations, string tgt)
        {
            //no spawn
            switch (relations)
            {
                case "conditions":
                    return new Relation(src, Arrow.Condition, tgt);
                case "responses":
                    return new Relation(src, Arrow.Response, tgt);
                case "excludes":
                    return new Relation(src, Arrow.Exclude, tgt);
                case "includes":
                    return new Relation(src, Arrow.Include, tgt);
                case "milestones":
                    return new Relation(src, Arrow.Milestone, tgt);
                default:
                    return null;

            }
        }

        //recussively get the nest groups
        private static void getEvents(XmlElement events, string parent,
                                Dictionary<string, List<string>> eventDic)
        {

            if (events.ChildNodes.Count > 0)
            {
                List<string> NewEvents = new List<string>();
                foreach (var child in events.ChildNodes)
                {
                    if (!child.GetType().Equals(typeof(XmlElement)))
                        continue;
                    var xmlC = (XmlElement)child;
                    if (xmlC.Name.Equals("event") || xmlC.Name.Equals("events"))
                        NewEvents.Add(xmlC.GetAttribute("id"));
                }
                if (NewEvents.Count > 0)
                    eventDic.Add(parent, NewEvents);
                foreach (var child in events.ChildNodes)
                {
                    if (!child.GetType().Equals(typeof(XmlElement)))
                        continue;
                    var xmlC = (XmlElement)child;
                    if (xmlC.Name.Equals("event") || xmlC.Name.Equals("events"))
                        getEvents(xmlC, xmlC.GetAttribute("id"), eventDic);
                }

            }
        }


        //let parser do it or just put it here
        //should be in the parser since we have no include here
        private static void initialEventProcess(EventProcess process,
                                                HashSet<Relation> relations,
                                                Dictionary<string, List<string>> dic)
        {
            foreach (var relation in relations)
            {
                if (dic.ContainsKey(relation.src))
                    foreach (var funcName in dic[relation.src])
                        process.addRelation(funcName, relation.rel, relation.tgt);
                else if (dic.ContainsKey(relation.tgt))
                    foreach (var funcName in dic[relation.tgt])
                        process.addRelation(relation.src,
                                                relation.rel, funcName);
                else
                    process.addRelation(relation.src, relation.rel, relation.tgt);
            }
        }

        private static HashSet<string> allFuncNames(Dictionary<string, List<string>> dic)
        {
            var NameofFunctions = new HashSet<string>();
            foreach (var functionName in dic)
            {
                //if (functionName.Key == "root")
                //    continue;

                //NameofFunctions.Add(functionName.Key);//key is function name
                //some name is the nest name, do we need that???
                foreach (var innerFuncName in dic[functionName.Key])
                {
                    if (dic.ContainsKey(innerFuncName))
                        continue;
                    NameofFunctions.Add(innerFuncName);//values are function names as well
                }
            }
            return NameofFunctions;
        }
    }
}
*/