﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GrainInterfaces;
using System.Data.SqlClient;
using Orleans;
using Orleans.Runtime;
using Microsoft.Extensions.Logging;

namespace Grains
{
	public class CommonTools : Orleans.Grain, ICommonTools //, IOutgoingGrainCallFilter//, IIncomingGrainCallFilter
	{
		Dictionary<int, string> TransToString = new Dictionary<int, string>();
		Dictionary<int, string> AccountStateToString = new Dictionary<int, string>();
		Dictionary<int, string> IsCompleteToString = new Dictionary<int, string>();

		private string connString = 
			@"Data Source=.\SQLEXPRESS;Initial Catalog=Applications;
							Integrated Security=True;Pooling=False";
		private readonly ILogger logger;


		public CommonTools(ILogger<CommonTools> logger)
		{
			this.logger = logger;
		}
		
		//Nullable<int>
		

		public Task<bool> UpdateInfo(BasicInfo basicInfo, int index)
		{
			//, AccountNum = '{basicInfo.accountNum}'
			string sql_update = String.Format($"UPDATE Applicant SET " +
				$"Fund='{basicInfo.Fund}', Reason = '{basicInfo.Reason}', State='{0}'" +
				$" WHERE ApplicantId ='{index}'");
			using (SqlConnection conn = new SqlConnection(connString))
			{
				//return Task.FromResult(true);
				conn.Open();
				var comm = new SqlCommand(sql_update, conn);
				int n = comm.ExecuteNonQuery();
				conn.Close();
				
				if (n<0)
				{
					return Task.FromResult(false);
				}
				else
				{
					return Task.FromResult(true);
				}
				
			}

		}

		public Task<(int,int)> FindReview(int index)
		{
			string sql = String.Format($"SELECT * FROM Reviewer WHERE ReviewId='{index}'");

			using (SqlConnection conn = new SqlConnection(connString))
			{
				conn.Open();
				SqlCommand comm = new SqlCommand(sql, conn);
				SqlDataReader reader = comm.ExecuteReader();
				if (reader.Read())
				{
					var reviewTitle = reader.GetInt32(1);
					//basicInfo.Name = reader.GetString(1).Trim();
					var reviewPhase = reader.GetInt32(3);

					reader.Close();
					return Task.FromResult((reviewTitle,reviewPhase));
				}
				else
				{
					//bug估计在这里 不能为null
					reader.Close();
					return Task.FromResult((0, 0));
				}
				//reader.Close();
			}
			//return Task.FromResult(basicInfo);
		}


		public Task<BasicInfo> FindApplicant(int index, string ReviewerName)//, string test
		{


			BasicInfo basicInfo = new BasicInfo();
			string sql = String.Format($"SELECT * FROM Applicant WHERE ApplicantId='{index}'");

			using (SqlConnection conn = new SqlConnection(connString))
			{
				conn.Open();
				SqlCommand comm = new SqlCommand(sql, conn);
				SqlDataReader reader = comm.ExecuteReader();
				if (reader.Read())
				{
					
					//basicInfo.Name = reader.GetString(1).Trim();
					basicInfo.Fund = reader.GetInt32(3);
					basicInfo.Reason = reader.GetString(4).Trim();
					var state = reader.GetInt32(5);
					basicInfo.States = TransToString(state);

					//var test = reader.GetByte(6);
					basicInfo.Reviews.LayerReview = reader.GetByte(6) != 0;
					basicInfo.Reviews.ArchitectReview = reader.GetByte(7) != 0;
					basicInfo.Reviews.Review3 = reader.GetByte(8) != 0;
					basicInfo.Reviews.Review4 = reader.GetByte(9) != 0;
					basicInfo.accountNum = reader.GetString(10).Trim();
					basicInfo.isCompleted = IsCompleteToString(reader.GetInt32(11));
					var Accountstate = reader.GetInt32(12);
					basicInfo.AccountNumState = AccountStateToString(Accountstate);
					//reader.Close();
					//return Task.FromResult(basicInfo);


					reader.Close();
					return Task.FromResult(basicInfo);
				}
				else
				{
					//bug估计在这里 不能为null
					
					reader.Close();
					//logger.LogInformation("wrong!");
					basicInfo = null;
					return Task.FromResult(basicInfo);
				}
				//reader.Close();
			}
			//return Task.FromResult(basicInfo);
		}


		


		public Task<bool> UpdateState(int State, int index)
		{
			string sql_update = String.Format($"UPDATE Applicant SET " +
				$"State='{State}'" +
				$" WHERE ApplicantId ='{index}'");
			using (SqlConnection conn = new SqlConnection(connString))
			{
				//return Task.FromResult(true);
				conn.Open();
				var comm = new SqlCommand(sql_update, conn);
				int n = comm.ExecuteNonQuery();
				conn.Close();

				if (n < 0)
				{
					return Task.FromResult(false);
				}
				else
				{
					return Task.FromResult(true);
				}

			}

		}


		public void ForTest()
		{
			logger.Info("Invoke Success!!");
		}

		public void InitializaDictionaries()
		{
			TransToString[-1] = "not submitted";
			TransToString[0] = "already submitted";
			TransToString[1] = "reviewer1";
			TransToString[2] = "second review";
			TransToString[3] = "board review";
			TransToString[4] = "preApproved";
			TransToString[5] = "Approved";
			TransToString[6] = "Reject";

			AccountStateToString[0] = "not ready";
			AccountStateToString[1] = "submitted";
			AccountStateToString[2] = "changed the account, wait for approving";
			AccountStateToString[3] = "approved the change";
			AccountStateToString[4] = "rejected the change";

			IsCompleteToString[0] = "not ready";
			IsCompleteToString[1] = "Completed!";
			IsCompleteToString[2] = "you abort";
		}

		public async Task Invoke(IOutgoingGrainCallContext context)
		{
			//logger.Info("before ke hai xing");
			await context.Invoke();
			//logger.Info("ke hai xing");
			//logger.Info($"interface method name {context.InterfaceMethod.Name}");
			//logger.Info($"context.Result {context.Result is BasicInfo}");
			if (string.Equals(context.InterfaceMethod.Name, nameof(CommonTools.FindApplicant)))
			{
				logger.Info("in common tools findapplication");
				var basicInfo = (BasicInfo)context.Result;
				if (basicInfo.States == "Reject")
				{
					logger.Info("in reject");
					context.Result = null;
				}
			}
		}
	}
}
