﻿using GrainInterfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Grains
{
	class ReviewerEvents
	{
		private ICommonTools commonTools;
		private IAccountNum accountNum;
		public async Task<bool> Reviewer1Appove(int applicantIndex)
		{
			//var commonTools = client.GetGrain<ICommonTools>(0);
			var update = await commonTools.UpdateState(2, applicantIndex);
			return update;

		}

		public async Task<bool> Reviewer1Reject(int applicantIndex)
		{
			var update = await commonTools.UpdateState(6, applicantIndex);
			return update;
		}


		public async Task<bool> LawyerReviewApprove(int applicantIndex)
		{
			var update = await commonTools.UpdateState(3, applicantIndex);
			return update;
		}

		public async Task<bool> LawyerReviewReject(int applicantIndex)
		{
			var update = await commonTools.UpdateState(6, applicantIndex);
			return update;
		}

		public async Task<bool> ArchitectReviewApprove(int applicantIndex) 
		{
			var update = await commonTools.UpdateState(3, applicantIndex);
			return update;
		}


		public async Task<bool> ArchitectReviewReject(int applicantIndex)
		{
			var update = await commonTools.UpdateState(6, applicantIndex);
			return update;
		}


		public async Task<bool> Reviewer3Approve(int applicantIndex)
		{
			var update = await commonTools.UpdateState(3, applicantIndex);
			return update;
		}


		public async Task<bool> Reviewer3Reject(int applicantIndex)
		{
			var update = await commonTools.UpdateState(6, applicantIndex);
			return update;
		}

		public async Task<bool> Reviewer4Approve(int applicantIndex)
		{
			var update = await commonTools.UpdateState(3, applicantIndex);
			return update;
		}


		public async Task<bool> Reviewer4Reject(int applicantIndex)
		{
			var update = await commonTools.UpdateState(6, applicantIndex);
			return update;
		}
		public async Task<bool> BoardMeetingPreApprove(int applicantIndex)
		{
			var update = await commonTools.UpdateState(4, applicantIndex);
			return update;
		}

		public async Task<bool> BoardMeetingPreReject(int applicantIndex)
		{
			var update = await commonTools.UpdateState(6, applicantIndex);
			return update;
		}

		public async Task<bool> BoardMeetingApprove(int applicantIndex)
		{
			var update = await commonTools.UpdateState(5, applicantIndex);
			return update;
		}

		public async Task<bool> BoardMeetingReject(int applicantIndex)
		{
			var update = await commonTools.UpdateState(6, applicantIndex);
			return update;
		}



		public async Task<bool> AccountantApprove(string cardNum, int applicantIndex)
		{
			bool response = await accountNum.ChangeAccountNumState(cardNum,
															applicantIndex, 3);
			return response;
		}

		public async Task<bool> AccountantReject(string cardNum, int applicantIndex)
		{
			bool response = await accountNum.ChangeAccountNumState(cardNum,
															applicantIndex, 4);
			return response;
		}

		public async Task<bool> CompleteState(int applicantIndex)
		{
			var update = await accountNum.ChangeCompleteState(applicantIndex, 1);
			return update;
		}
	}
}
