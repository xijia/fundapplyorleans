﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GrainInterfaces;
using Orleans;
using Orleans.Providers;

namespace Grains
{
	[StorageProvider]//default
	public class HelloGrain : Grain<GreetingArchive>, IHelloGrain
	{
		
		public async Task<string> SayHello(string greeting)
		{
			//how to save
			State.Greetings.Add(greeting);
			await WriteStateAsync();//save to state

			var primaryKey = this.GetPrimaryKeyLong();

			Console.WriteLine($"this is primary key: {primaryKey}");
			return $"You said: {greeting}, I say : Hello";
		}
	}

	public class GreetingArchive
	{
		public List<string> Greetings { get; private set; } = new List<string>();
	}

}
