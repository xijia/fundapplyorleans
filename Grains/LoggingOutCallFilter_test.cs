﻿using GrainInterfaces;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Runtime;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Grains
{
    public class MethodUseStates
    {
        //所有ICommond的函数？
        bool FoundApplicant;
        //人物关系？
        int character;//不同数字代表不同的人
        Guid guid;
        int id;
        public MethodUseStates()
        {
            FoundApplicant = false;
        }

    }//还是struct呢
    public class LoggingOutCallFilter_test// : IOutgoingGrainCallFilter
    {
        private readonly ILogger log;
        private readonly IGrainFactory grainFactory;

        //public LoggingOutCallFilter(ILogger<LoggingCallFilter> logger)
        //{
        //    this.log = logger;

        //}
        public LoggingOutCallFilter_test(IGrainFactory grainFactory)
        {
            this.grainFactory = grainFactory;

        }

        public async Task Invoke(IOutgoingGrainCallContext context)
        {

            await context.Invoke();
            try
            {
               FilterReject(context);
                GetCorrectPhase(context);
                await CallOrtherFunction(context);
            }
            catch (Exception exception)
            {
                var msg = string.Format(
                    "{0}.{1}({2}) threw an exception: {3}",
                    context.Grain.GetType(),
                    context.InterfaceMethod.Name,
                    context.Arguments,
                    exception);
                this.log.Info(msg);

                // If this exception is not re-thrown, it is considered to be
                // handled by this filter.
                throw;
            }



        }

        private void FilterReject(IOutgoingGrainCallContext context)
        {
            //现在问题是 到时候我编译的时候 这个函数名咋个定？？？
            if (string.Equals(context.InterfaceMethod.Name, nameof(CommonTools.FindApplicant)))
            {
                var reviewerOrApplicant = context.Arguments.GetValue(1);
                if (!string.Equals((string)reviewerOrApplicant, "applicant"))
                {
                    var basicInfo = (BasicInfo)context.Result;
                    if (basicInfo.States == "Reject")
                    {
                        context.Result = null;
                    }
                }
            }

        }

       
        private void GetCorrectPhase(IOutgoingGrainCallContext context)
        {
            //现在问题是 到时候我编译的时候 这个函数名咋个定？？？
            if (string.Equals(context.InterfaceMethod.Name, nameof(CommonTools.FindApplicant)))
            {
                var reviewerOrApplicant = context.Arguments.GetValue(1);
                //我在想要不要写两种调用的
                if (!string.Equals((string)reviewerOrApplicant, "applicant"))
                {
                    
                    var basicInfo = (BasicInfo)context.Result;
                    //this.log.Info(basicInfo.States);
                    //看看阶段是否一致
                    if (!string.Equals(basicInfo.States, reviewerOrApplicant))
                    {
                        context.Result = null;
                    }
                }
            }

        }

        private async Task CallOrtherFunction(IOutgoingGrainCallContext context)
        {
            //现在问题是 到时候我编译的时候 这个函数名咋个定？？？
            if (string.Equals(context.InterfaceMethod.Name, nameof(CommonTools.FindApplicant)))
            {
                
                var reviewerOrApplicant = context.Arguments.GetValue(1);
                if (string.Equals((string)reviewerOrApplicant, "reviewer1"))
                {
                    //var index = context.Arguments.GetValue(0);
                    //var basicInfo = (BasicInfo)context.Result;
                    //basicInfo.States = "testetstest";
                    var test = this.grainFactory.GetGrain<ICommonTools>(0);
                    test.ForTest();//basicInfo, (int)index
                }
            }

        }
    }
}
