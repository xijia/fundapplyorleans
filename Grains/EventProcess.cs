﻿using System;
using System.Collections.Generic;
using System.Text;
using GrainInterfaces;
using Orleans;
using System.Xml;
using System.Threading.Tasks;
using Orleans.Streams;
using Microsoft.Extensions.Logging;

namespace Grains
{


    //感觉有点危险 好像可以被修改
    public class EventProcess : Orleans.Grain, IEventProcess
    {
        //所有events的名字
        private HashSet<string> events = new HashSet<string>();
        private HashSet<Relation> relations = new HashSet<Relation>();

        public HashSet<string> executed = new HashSet<string>();
        public HashSet<string> included = new HashSet<string>();
        public HashSet<string> pending = new HashSet<string>();
        private readonly ILogger log;
        private IAsyncStream<EventMsg> stream;
        public Guid myGuid = new Guid();
        Orleans.Streams.IStreamProvider streamProvider;
        public override Task OnActivateAsync()
        {
            log.LogInformation("in OnActivateAsync");
            streamProvider = GetStreamProvider("ForTest");

            stream = streamProvider.GetStream<EventMsg>(myGuid,
                                            this.GetPrimaryKeyString());
            return base.OnActivateAsync();
        }

        //string DCRFileName先不要搞带参数的了
        //@"C:\orleans\simpleExample\simpleTest2.xml"要变成配置类
        public EventProcess(ILogger<LoggingCallFilter_test> logger)
        {
            log = logger;
            log.LogInformation("in EventProcess");
            
            log.LogInformation("log guid" + myGuid.ToString());
            parseXML(@"C:\orleans\simpleExample\simpleTest2.xml");
        }

        public Task<HashSet<string>> GetExecuted()
        {
            //觉得这么写怪怪的...
            return Task.FromResult(executed);
        }
        public Task<Guid> GetGuid()
        {
            //觉得这么写怪怪的...
            return Task.FromResult(stream.Guid);
        }

        public Task<Orleans.Streams.IStreamProvider> TestProvider()
        {
            //觉得这么写怪怪的...
            return Task.FromResult(streamProvider);
        }
        public Task<HashSet<string>> GetIncluded()
        {
            return Task.FromResult(included);
        }

        public Task<HashSet<string>> GetPending()
        {
            return Task.FromResult(pending);
        }

        /* Add an event to the graph.The event will be not executed, included,
             * and not pending.*/


        public void addEvent(String name)
        {
            events.Add(name);
            included.Add(name);
        }

        /* Add a relation to the graph.Events are assumed to exist already.*/


        public void addRelation(String src, Arrow arr, String tgt)
        {
            relations.Add(new Relation(src, arr, tgt));
        }




        /*Run-time.

        Return the set of enabled events.*/
        public Task<HashSet<string>> enabled()
        {
            var result = new HashSet<string>(included);

            foreach (var r in relations)
            {
                switch (r.rel)
                {
                    case Arrow.Condition:
                        if (included.Contains(r.src) && !executed.Contains(r.src))
                            result.Remove(r.tgt);
                        break;

                    case Arrow.Milestone:
                        if (included.Contains(r.src) && pending.Contains(r.src))
                            result.Remove(r.tgt);
                        break;

                    default:
                        break;
                }
            }
            //onBatchNext? wow
            
            return Task.FromResult(result);
        }

        /*
                Execute event e.Assumes e is enabled.*/
        public void execute(string e)
        {
            pending.Remove(e);
            executed.Add(e);
            foreach (var r in relations)
            {
                if (r.src != e)
                    continue;

                switch (r.rel)
                {
                    case Arrow.Exclude:
                        included.Remove(r.tgt);
                        break;

                    case Arrow.Include:
                        included.Add(r.tgt);
                        break;

                    case Arrow.Response:
                        pending.Add(r.tgt);
                        break;

                    default:
                        break;
                }
            }
        }

        /*a function needed by the send event*/
        public async Task<bool> eventExcuted(string eventHappend)
        {
            execute(eventHappend);
            var result = await enabled();
            await stream.OnNextAsync(new EventMsg(result, this.GetPrimaryKeyString()));
            return true;
        }


        public Task<bool> isAccepting()
        {
            /*
            foreach (var e in pending)
            {
                if (included.Contains(e))
                    return Task.FromResult(false);
            }*/
            return Task.FromResult(true);
        }

        //下面都改成异步的
        public void parseXML(string file)
        {
            //@"C:\orleans\FundApplyOrleans\With_Constrains.xml"
            XmlDocument xmlDoc = new XmlDocument();
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreComments = true;
            XmlReader reader = XmlReader.Create(file, settings);
            xmlDoc.Load(reader);
            var allGroups = new Dictionary<string, List<string>>();//key is the parent
            HashSet<Relation> relations = new HashSet<Relation>();
            //HashSet<string> includedFunc = new HashSet<string>();
            //var process = new EventProcess();
            //acquire the group/nesting
            foreach (XmlElement events in xmlDoc.SelectNodes(@"dcrgraph/specification/resources/events"))
            {
                getEvents(events, "root", allGroups);
            }
            //included
            foreach (XmlElement eventsInclude in xmlDoc.SelectNodes(@"dcrgraph/runtime/marking/included"))
            {
                foreach (XmlElement eventid in eventsInclude)
                {
                    //includedFunc.Add(eventid.GetAttribute("id"));
                    var id = eventid.GetAttribute("id");
                    if (allGroups.ContainsKey(id))
                        continue;
                    addEvent(id);
                }
            }
            //constrains
            foreach (XmlElement constraints in xmlDoc.SelectNodes(@"dcrgraph/specification/constraints"))//resources/events/event"))
            {
                foreach (XmlElement conditions in constraints.ChildNodes)
                {
                    foreach (XmlElement cdt in conditions.ChildNodes)
                    {

                        relations.Add(findRelations(cdt.GetAttribute("sourceId"),
                                      conditions.Name, cdt.GetAttribute("targetId")));
                    }
                }
            }

            reader.Close();

            initialEventProcess(relations, allGroups);
            //allNames 应该就是 events
            /* var allNames = allFuncNames(allGroups);

             return allNames;*/
        }

        private static Relation findRelations(string src, string relations, string tgt)
        {
            //no spawn
            switch (relations)
            {
                case "conditions":
                    return new Relation(src, Arrow.Condition, tgt);
                case "responses":
                    return new Relation(src, Arrow.Response, tgt);
                case "excludes":
                    return new Relation(src, Arrow.Exclude, tgt);
                case "includes":
                    return new Relation(src, Arrow.Include, tgt);
                case "milestones":
                    return new Relation(src, Arrow.Milestone, tgt);
                default:
                    return null;

            }
        }

        //recussively get the nest groups
        private static void getEvents(XmlElement events, string parent,
                                Dictionary<string, List<string>> eventDic)
        {

            if (events.ChildNodes.Count > 0)
            {
                List<string> NewEvents = new List<string>();
                foreach (var child in events.ChildNodes)
                {
                    if (!child.GetType().Equals(typeof(XmlElement)))
                        continue;
                    var xmlC = (XmlElement)child;
                    if (xmlC.Name.Equals("event") || xmlC.Name.Equals("events"))
                        NewEvents.Add(xmlC.GetAttribute("id"));
                }
                if (NewEvents.Count > 0)
                    eventDic.Add(parent, NewEvents);
                foreach (var child in events.ChildNodes)
                {
                    if (!child.GetType().Equals(typeof(XmlElement)))
                        continue;
                    var xmlC = (XmlElement)child;
                    if (xmlC.Name.Equals("event") || xmlC.Name.Equals("events"))
                        getEvents(xmlC, xmlC.GetAttribute("id"), eventDic);
                }

            }
        }


        //let parser do it or just put it here
        //should be in the parser since we have no include here
        private void initialEventProcess(HashSet<Relation> relations,
                                                Dictionary<string, List<string>> dic)
        {
            foreach (var relation in relations)
            {
                if (dic.ContainsKey(relation.src))
                    foreach (var funcName in dic[relation.src])
                        addRelation(funcName, relation.rel, relation.tgt);
                else if (dic.ContainsKey(relation.tgt))
                    foreach (var funcName in dic[relation.tgt])
                        addRelation(relation.src,
                                                relation.rel, funcName);
                else
                    addRelation(relation.src, relation.rel, relation.tgt);
            }
        }


    }
}

