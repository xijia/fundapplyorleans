﻿using System;
using System.Collections.Generic;
using System.Text;
using Orleans;
using GrainInterfaces;
using Orleans.Streams;
using System.Threading.Tasks;

namespace Grains
{
	[Serializable]
	public class EventMsg
	{
		public DateTimeOffset Created { get; set; } = DateTimeOffset.Now;
		public HashSet<string> IncludeTable { get; set; }
		public string UserId { get; set; }


/*		public EventMsg()
		{
			//是不是要加点什么
		}*/

		public EventMsg(HashSet<string> includeTable, string userId)
		{
			IncludeTable = includeTable;
			UserId = userId;
		}
	}

	public class StreamingTest : Grain, IStreamingTest
	{
		private IAsyncStream<EventMsg> stream;
		private readonly List<EventMsg> eventHistory = new List<EventMsg>(100);
		public override Task OnActivateAsync()
		{
			var streamProvider = GetStreamProvider("StreamingTest");

			stream = streamProvider.GetStream<EventMsg>(Guid.NewGuid(), "ForTest");
			return base.OnActivateAsync();
		}

		public async Task<bool> EventMessage(EventMsg msg)
		{
			eventHistory.Add(msg);//暂时好像没有用
			await stream.OnNextAsync(msg);

			return true;
		}
	}
}
