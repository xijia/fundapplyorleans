﻿using GrainInterfaces;
using Microsoft.Extensions.Logging;
using GrainInterfaces;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Runtime;
using Orleans.Streams;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
//using static Grains.EventProcess;

namespace Grains
{
    //不知道加了grain的是不是全部的 还是pergrain 如何用
    public class OutGoingCallFilter : Grain, IOutgoingGrainCallFilter
    {
        private readonly ILogger log;
        private readonly IGrainFactory grainFactory;
        //如果我用guid 要是之后的不用了怎么办 要做回收吗

        //static public void Show(EventProcess.Process P)
        //{
        //    Console.WriteLine("    Enabled: {0}", String.Join(", ", P.enabled()));
        //    Console.WriteLine("    Accepting: {0}", P.isAccepting());
        //}

        //static public void Execute(EventProcess.Process P, String e)
        //{
        //    Console.WriteLine(". Executing {0}", e);
        //    P.execute(e);
        //    Show(P);
        //}

        //static public void Header(EventProcess.Process P, String title)
        //{
        //    Console.WriteLine("* New graph: {0}", title);
        //    Show(P);
        //}
        private IAsyncStream<EventMsg> stream;

        public void GiveStream(Guid guid, string nameSpace)
        {
            var streamProvider = GetStreamProvider("TestSthProvider");

            stream = streamProvider.GetStream<EventMsg>(guid, nameSpace);

        }
        public async Task Invoke(IOutgoingGrainCallContext context)
        {
            await context.Invoke();
            var guid = context.Grain.GetPrimaryKey();
            

            try
            {

            }
            catch (Exception exception)
            {
                var msg = string.Format(
                    "{0}.{1}({2}) threw an exception: {3}",
                    context.Grain.GetType(),
                    context.InterfaceMethod.Name,
                    "1",
                    exception);
                this.log.Info(msg);

                // If this exception is not re-thrown, it is considered to be
                // handled by this filter.
                throw;
            }
        }
    }
}
