﻿using GrainInterfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace Grains
{
	class AccountNum : Orleans.Grain, IAccountNum
	{
		private string connString =
			@"Data Source=.\SQLEXPRESS;Initial Catalog=Applications;
							Integrated Security=True;Pooling=False";


		public Task<bool> ChangeAccountNumState(string accountNum, int index, int State) 
		{
			var sql_update = String.Format($"UPDATE Applicant SET " +
				  $"AccountNum='{accountNum}', AccountNumState='{0}'" +
				  $" WHERE ApplicantId ='{index}'");
			if (State == 2)//changed
			{
				sql_update = String.Format($"UPDATE Applicant SET " +
				  $"AccountNumChanged='{accountNum}', AccountNumState='{State}'" +
				  $" WHERE ApplicantId ='{index}'");
			}
			else//reject State == 3 submit 1
			{
				sql_update = String.Format($"UPDATE Applicant SET " +
				  $"AccountNum='{accountNum}', AccountNumState='{State}'" +
				  $" WHERE ApplicantId ='{index}'");
				
			}
			using (SqlConnection conn = new SqlConnection(connString))
			{
				//return Task.FromResult(true);
				conn.Open();
				var comm = new SqlCommand(sql_update, conn);
				int n = comm.ExecuteNonQuery();
				conn.Close();

				if (n < 0)
				{
					return Task.FromResult(false);
				}
				else
				{
					return Task.FromResult(true);
				}

			}
		}
		//approve 2
		public Task<bool> ApproveChange(string accountNumChanged, 
										int index)
		{
			string sql_update = String.Format($"UPDATE Applicant SET " +
				$"AccountNum='{accountNumChanged}', AccountNumState='2'" +
				$" WHERE ApplicantId ='{index}'");
			using (SqlConnection conn = new SqlConnection(connString))
			{
				//return Task.FromResult(true);
				conn.Open();
				var comm = new SqlCommand(sql_update, conn);
				int n = comm.ExecuteNonQuery();
				conn.Close();

				if (n < 0)
				{
					return Task.FromResult(false);
				}
				else
				{
					return Task.FromResult(true);
				}

			}


			
		}
		//ApplicantAbort 3
		//complete 1
		//not complete 0
		public Task<bool> ChangeCompleteState(int index,int state)
		{
			string sql_update = String.Format($"UPDATE Applicant SET " +
				$"FinalDecision='{state}'" +
				$" WHERE ApplicantId ='{index}'");
			using (SqlConnection conn = new SqlConnection(connString))
			{
				//return Task.FromResult(true);
				conn.Open();
				var comm = new SqlCommand(sql_update, conn);
				int n = comm.ExecuteNonQuery();
				conn.Close();

				if (n < 0)
				{
					return Task.FromResult(false);
				}
				else
				{
					return Task.FromResult(true);
				}

			}
		}



	}
}
