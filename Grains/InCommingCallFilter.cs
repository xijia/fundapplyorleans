﻿using GrainInterfaces;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Runtime;
using Orleans.Streams;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Grains
{
    //do not choose the InCommingCallFilter in solihost, why?
    //1. every internal call will be invoked every time
    //2. not per grain, which means there will be a bottleneck, messages block here
    //3. cannot use the streamProvider, has the problem : Grain 
    // was created outside of the Orleans creation process and no runtime was specified.
    // tried to get the provider outside, like call a grain to get the exist streamProvider,
    //still failed

    public class InCommingCallFilter : Grain, IIncomingGrainCallFilter//可以加grain吗
    {
        private readonly ILogger log;
        private Dictionary<string, HashSet<string>> userNameWithEvents;
        private Dictionary<string, Guid> userNameWithGuid;
        private HashSet<string> nameOfFunctions;
        private readonly IGrainFactory grainFactory;
        Orleans.Streams.IStreamProvider streamProvider;
/*
        public override Task OnActivateAsync()
        {
            log.LogInformation("in this OnActivateAsync");
            streamProvider = GetStreamProvider("ForTest");

            return base.OnActivateAsync();
        }*/
        //这个不能放在incoming里面？？？

        //bottle neck就bottleneck吧 救了命了

        /*        private IAsyncStream<EventMsg> stream;

                public void GiveStream(Guid guid, string nameSpace)
                {
                    var streamProvider = GetStreamProvider("TestSthProvider");

                    stream = streamProvider.GetStream<EventMsg>(guid, nameSpace);

                }*/

        /*        public override Task OnActivateAsync()
                {
                    log.LogInformation("in here OnActivateAsync");
                    streamProvider = base.GetStreamProvider("ForTest");
                    return base.OnActivateAsync();
                }*/

        public InCommingCallFilter(IGrainFactory grainFactory, ILogger<InCommingCallFilter> logger)
        {
            this.grainFactory = grainFactory;
            //在IncomCallFilter里不能获取provider???
            //var streamProvider = GetStreamProvider("ForTest");
            //ComeIn = false;
            this.log = logger;/*grainFactory.GetGrain<ILogger>();*/
            userNameWithEvents = new Dictionary<string, HashSet<string>>();
            userNameWithGuid = new Dictionary<string, Guid>();//这个guid是stream要用
            nameOfFunctions = new HashSet<string>();
            /* var getInitialEventGrain = GrainFactory.GetGrain<IEventProcess>(new Guid(), "systemBasic");
             NameofFunctions = await getInitialEventGrain.GetIncluded();*/
            getInitialEventFromXML(@"C:\orleans\simpleExample\simpleTest2.xml");
            foreach (var test in nameOfFunctions)
                log.LogInformation($"NameofFunctions + {test}");
        }

/*        public async void initialSubscribStreamSth(Guid streamId, string nameSpace)
        {
            var HereStream = GetStreamProvider("TestSthProvider").GetStream<EventMsg>(streamId, nameSpace);
            //subscribe to the stream to receiver furthur messages sent to the chatroom
            await HereStream.SubscribeAsync(new StreamObserver(log));
        }*/

        //我这个是siloHost注册的 所以就
        public async Task Invoke(IIncomingGrainCallContext context)
        {
            var test = streamProvider = base.GetStreamProvider("ForTest");
            log.LogInformation("in Invoke");
            await context.Invoke();
/*
            //    var keyString = context.Grain.GetPrimaryKey();
            var incomingFunctionName = context.InterfaceMethod.Name;
            if (!nameOfFunctions.Contains(incomingFunctionName))
            {
                await context.Invoke();
            }
            else
            {

                var primaryString = context.Grain.GetPrimaryKeyString();
                if (userNameWithEvents.ContainsKey(primaryString))
                {
                    //update
                    if (userNameWithEvents[primaryString].Contains(incomingFunctionName))
                        await context.Invoke();
                    else
                        throw new Exception(
                        string.Format(
                        "Exception of {0} cannot be process since it is not included.",
                        incomingFunctionName
                        ));
                }
                else
                {
                    
                    var eventGrain = grainFactory.GetGrain<IEventProcess>(primaryString);
                    
                    var streamId = await eventGrain.GetGuid();
                    //streamProvider = await eventGrain.TestProvider();
                    log.LogInformation(streamId.ToString());
                    var HereStream = GetStreamProvider("ForTest").GetStream<EventMsg>(streamId, primaryString);
                    //subscribe to the stream to receiver furthur messages sent to the chatroom
                    //var HereStream = streamProvider.GetStream<EventMsg>(streamId, primaryString);
                    await HereStream.SubscribeAsync(new StreamObserver(log));
                    userNameWithEvents.Add(primaryString, nameOfFunctions);
                    await context.Invoke();
*//*                    streamProvider = base.GetStreamProvider("ForTest");
                    var HereStream = streamProvider.GetStream<EventMsg>(streamId, primaryString);
                    await HereStream.SubscribeAsync(new StreamObserver(log));*//*

                    //log.LogInformation($"sth + {context.InterfaceMethod.Name}");

                    //这个grain是什么才能get什么！！！component的还不一样
                    var userId = context.Grain.GetPrimaryKeyString();
                    log.LogInformation($"sth + {userId}");
                    
                }
                
               *//* await stream.OnNextAsync(new EventMsg(context.InterfaceMethod.Name, uid));*//*
            }*/
            //首先看这个guid的instance在不在我的字典里
            /*else
            {
                if (ComeIn)
                {
                    // if it exists, then we get the process of it

                    var interfaceName = context.InterfaceMethod.Name;
                    //check if the method is enable in our process
                    if (process.enabled().Contains(interfaceName))
                    {
                        //if it is enable then we invoke

                        //after invoking, we should excute the relation of this method/event
                        process.execute(interfaceName);
                        //update then include as the enable ones, should I?
                        //because it is already updated in the excuted hashset
                        //process.included = process.enabled();
                    }
                    else
                    {
                        //else we throw exception
                        throw new Exception(
                        string.Format(
                        "Exception of {0} cannot be process since it is not included.",
                        interfaceName
                        ));
                    }
                }
                //不能获取guid太麻烦了吧 我局的可以的 要在特定的函数
                else if (string.Equals(context.InterfaceMethod.Name, nameof(IsmallExample.FillOutAnExpenseReport)))
                {
                    log.LogInformation($"sth");
                    //新建instance的process
                    //guidWithProcess.Add(keyString, intialProcess());
                    //执行第一个关系
                    //var process = guidWithProcess[keyString];
                    //process = intialProcess();
                    process.execute(context.InterfaceMethod.Name);
                    ComeIn = true;
                    //process.included = process.enabled();
                }
            }*/
        }




        /* //public HashSet<Guid> EventsProcess = new HashSet<Guid>();
         private readonly ILogger log;
         //private bool ComeIn;
         private EventProcess process;
         //private List<string> NameofFunctions;
         private HashSet<string> NameofFunctions;//not support the same event/function name

         //Dictionary<Guid, EventProcess.Process> guidWithProcess = new Dictionary<Guid, EventProcess.Process>();
         //public HashSet<EventProcess.Process> EventsProcess = new HashSet<EventProcess.Process>();
         public InCommingCallFilter(ILogger<LoggingCallFilter_test> logger)
         {
             //ComeIn = false;
             this.log = logger;
             parseXML parseXMLFiles = new parseXML();
             (NameofFunctions, process) = parseXML.parseXMLTester(@"C:\orleans\simpleExample\simpleTest2.xml");
             foreach (var test in NameofFunctions)
                 log.LogInformation($"NameofFunctions + {0}", test);
             //initialEventList();
         }
         *//*public async Task Invoke(IIncomingGrainCallContext context)
         {


             //    var keyString = context.Grain.GetPrimaryKey();
             //    log.LogInformation($"sth + {0}", keyString);
             if (!NameofFunctions.Contains(context.InterfaceMethod.Name))
             {
                 //log.LogInformation("not in the event function list");
                 await context.Invoke(); 
             }
             //首先看这个guid的instance在不在我的字典里
             else
             {
                 if (ComeIn)
                 {
                     // if it exists, then we get the process of it

                     var interfaceName = context.InterfaceMethod.Name;
                     //check if the method is enable in our process
                     if (process.enabled().Contains(interfaceName))
                     {
                         //if it is enable then we invoke

                         //after invoking, we should excute the relation of this method/event
                         process.execute(interfaceName);
                         //update then include as the enable ones, should I?
                         //because it is already updated in the excuted hashset
                         //process.included = process.enabled();
                     }
                     else
                     {
                         //else we throw exception
                         throw new Exception(
                         string.Format(
                         "Exception of {0} cannot be process since it is not included.",
                         interfaceName
                         ));
                     }
                 }
                 //不能获取guid太麻烦了吧 我局的可以的 要在特定的函数
                 else if (string.Equals(context.InterfaceMethod.Name, nameof(IsmallExample.FillOutAnExpenseReport)))
                 {
                     log.LogInformation($"sth");
                     //新建instance的process
                     //guidWithProcess.Add(keyString, intialProcess());
                     //执行第一个关系
                     //var process = guidWithProcess[keyString];
                     //process = intialProcess();
                     process.execute(context.InterfaceMethod.Name);
                     ComeIn = true;
                                               //process.included = process.enabled();
                 }
             }
         }
 *//*

         public async Task Invoke(IIncomingGrainCallContext context)
         {

         }

         //下面应该填dcr graph生成的那些关系
         private EventProcess intialProcess()
         {
             var newProcess = new EventProcess();
             initialEventProcess(newProcess);
             return newProcess;

         }

         private void initialEventList() 
         {
             NameofFunctions = new HashSet<string>();
             NameofFunctions.Add("FilloutAnExpenseReport");
             NameofFunctions.Add("WithdrawTheExpenseReport");

             NameofFunctions.Add("Reject");
             NameofFunctions.Add("Approve");
             NameofFunctions.Add("PayOut");
         }

         //generate relations
         private static void initialEventProcess(EventProcess newProcess)
         {
             //take the first enter as the guid?
             //比如 filloutapplicant就是第一个入口 就应该作为guid的？
             newProcess.addEvent("FilloutAnExpenseReport");
             newProcess.addEvent("WithdrawTheExpenseReport");

             newProcess.addEvent("Reject");
             newProcess.addEvent("Approve");
             newProcess.addEvent("PayOut");

             newProcess.addRelation("FilloutAnExpenseReport",
                                     Arrow.Condition,
                                     "PayOut");
             newProcess.addRelation("FilloutAnExpenseReport",
                                     Arrow.Condition,
                                     "Approve");
             newProcess.addRelation("Approve",
                                     Arrow.Condition,
                                     "PayOut");

             foreach (string _event in newProcess.included)
                 newProcess.addRelation("WithdrawTheExpenseReport",
                                         Arrow.Exclude,
                                         _event);
             foreach (string _event in newProcess.included)
                 newProcess.addRelation("PayOut",
                                         Arrow.Exclude,
                                         _event);

         }





     }*/
        //private readonly ILogger log;

        //public InCommingCallFilter(ILogger<InCommingCallFilter> logger)
        //{
        //    this.log = logger;

        //}
        //public async Task Invoke(IIncomingGrainCallContext context)
        //{
        //    try
        //    {
        //        await context.Invoke();
        //        var msg = string.Format(
        //            "{0}.{1}({2}) returned value {3}",
        //            context.Grain.GetType(),
        //            context.InterfaceMethod.Name,
        //            string.Join(", ", context.Arguments),
        //            context.Result);
        //        this.log.Info(msg);
        //    }
        //    catch (Exception exception)
        //    {
        //        var msg = string.Format(
        //            "{0}.{1}({2}) threw an exception: {3}",
        //            context.Grain.GetType(),
        //            context.InterfaceMethod.Name,
        //            string.Join(", ", context.Arguments),
        //            exception);
        //        this.log.Info(msg);

        //        // If this exception is not re-thrown, it is considered to be
        //        // handled by this filter.
        //        throw;
        //    }

        public void getInitialEventFromXML(string file)
        {
            //@"C:\orleans\FundApplyOrleans\With_Constrains.xml"
            XmlDocument xmlDoc = new XmlDocument();
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreComments = true;
            XmlReader reader = XmlReader.Create(file, settings);
            xmlDoc.Load(reader);
            var allGroups = new Dictionary<string, List<string>>();//key is the parent
            /*HashSet<string> allNames = new HashSet<string>();*/

            foreach (XmlElement events in xmlDoc.SelectNodes(@"dcrgraph/specification/resources/events"))
            {
                getEvents(events, "root", allGroups);
            }
            //included
            foreach (XmlElement eventsInclude in xmlDoc.SelectNodes(@"dcrgraph/runtime/marking/included"))
            {
                foreach (XmlElement eventid in eventsInclude)
                {
                    //includedFunc.Add(eventid.GetAttribute("id"));
                    var id = eventid.GetAttribute("id");
                    if (allGroups.ContainsKey(id))
                        continue;
                    nameOfFunctions.Add(id);
                }
            }

            reader.Close();   
        }

        private static void getEvents(XmlElement events, string parent,
                                Dictionary<string, List<string>> eventDic)
        {

            if (events.ChildNodes.Count > 0)
            {
                List<string> NewEvents = new List<string>();
                foreach (var child in events.ChildNodes)
                {
                    if (!child.GetType().Equals(typeof(XmlElement)))
                        continue;
                    var xmlC = (XmlElement)child;
                    if (xmlC.Name.Equals("event") || xmlC.Name.Equals("events"))
                        NewEvents.Add(xmlC.GetAttribute("id"));
                }
                if (NewEvents.Count > 0)
                    eventDic.Add(parent, NewEvents);
                foreach (var child in events.ChildNodes)
                {
                    if (!child.GetType().Equals(typeof(XmlElement)))
                        continue;
                    var xmlC = (XmlElement)child;
                    if (xmlC.Name.Equals("event") || xmlC.Name.Equals("events"))
                        getEvents(xmlC, xmlC.GetAttribute("id"), eventDic);
                }

            }
        }
    }

        public class StreamObserver : IAsyncObserver<EventMsg>
        {
			private ILogger logger;
			
			public StreamObserver(ILogger logger)
			{
				this.logger = logger;
			}

			public Task OnCompletedAsync()
			{
				this.logger.LogInformation("Message stream received stream completed event");
				return Task.CompletedTask;
			}

			public Task OnErrorAsync(Exception ex)
			{
				this.logger.LogInformation($"Message delivery failure, ex :{ex}");
				return Task.CompletedTask;
			}

			//如果订阅的只是当时的stream的话 得到的就是这个user的stream
			public Task OnNextAsync(EventMsg item, StreamSequenceToken token = null)
			{

				this.logger.LogInformation($"=={item.Created}==  userid {item.UserId}");
				foreach (var events in item.IncludeTable)
					this.logger.LogInformation($"include events {events}");
				//在这里做一下表演include的 不过一步一步来

				return Task.CompletedTask;
			}
		}
}


