﻿using System;
using System.Collections.Generic;
using System.Text;
using Orleans;
using GrainInterfaces;
using System.Threading.Tasks;

namespace Grains
{
	public class ApplicantEvents : Orleans.Grain, IApplicantEvents
	{
		private ICommonTools commonTools;
		private IAccountNum accountNum;//这个和给参数的有啥区别哦
		

		public async Task<bool> FillOutApplication(BasicInfo basicInfo, int insert_id)
		{
			bool response = await commonTools.UpdateInfo(basicInfo, insert_id);
			return response;
		}

		public async Task<bool> FirstPayment(string CardNum, int applicantID)
		{
			//var acc = GrainFactory.GetGrain<IAccountNum>(0);
			bool response = await accountNum.ChangeAccountNumState(CardNum, applicantID, 1);
			return response;
		}


		public async Task<bool> EditPayment(string CardNum, int applicantID)
		{
			bool response = await accountNum.ChangeAccountNumState(CardNum, applicantID, 2);
			return response;
		}


		public async Task<bool> AbortApply(int applicantID) 
		{
			var update = await accountNum.ChangeCompleteState(applicantID, 2);
			return update;
		}




	}
}
