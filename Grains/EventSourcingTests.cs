﻿using GrainInterfaces;
using Orleans.EventSourcing;
using Orleans.Providers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace Grains
{
	//JournaledGrain<>
	//GreetingState -> event state
	//GreetingEvent -> base event type
	//这个就是会记录下最新的state
	[LogConsistencyProvider(ProviderName = "StateStorage")]
	public class EventSourcingTests : JournaledGrain<GreetingState,GreetingEvent>, IEventSourcingTests
	{
		
		
		public async Task<string> SendGreetings(string greetings)
		{
			//这个就是最新的 从 state storage里面取出来的？
			var state = State.Greeting;//lastest state of the grain
									   //if we want to save states??
			RaiseEvent(new GreetingEvent { Greeting = greetings });
			//after raise event, the event we give will be applied
			//and save in the states (executed by apply methods)->
			//so does it mean we could use event check here or event update here
			await ConfirmEvents();//if you want to confirm
			return greetings;

		}


	}

	public class GreetingEvent
	{
		public string Greeting { get; set; }
	}

	//Apply is the method that every time 
	//you are going to the state, it would be executed
	//then the state will be saved
	public class GreetingState
	{
		public string Greeting { get; set; }

		public GreetingState Apply ( GreetingEvent @event)
		{
			Greeting = @event.Greeting;
			return this;
		}
	}

}
