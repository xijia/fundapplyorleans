﻿using GrainInterfaces;
using Orleans;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Grains
{
	public class CustomCallFilter  //:IIncomingGrainCallFilter
	{
        private readonly IGrainFactory grainFactory;

        public CustomCallFilter(IGrainFactory grainFactory)
        {
            this.grainFactory = grainFactory;
        }

        public async Task Invoke(IIncomingGrainCallContext context)
        {
            // Hook calls to any grain other than ICustomFilterGrain implementations.
            // This avoids potential infinite recursion when calling OnReceivedCall() below.
            if (!(context.Grain is ICommonTools))
            {
                var filterGrain = this.grainFactory.GetGrain<ICommonTools>(context.Grain.GetPrimaryKeyLong());

                // Perform some grain call here.
                await filterGrain.FindApplicant(0,"sth");
            }

            // Continue invoking the call on the target grain.
            await context.Invoke();
        }
    }
}
