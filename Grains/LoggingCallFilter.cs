﻿using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Runtime;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Grains
{
    public class LoggingCallFilter //: IIncomingGrainCallFilter
    {
        private readonly ILogger log;

        public LoggingCallFilter(ILogger<LoggingCallFilter> logger)
        {
            this.log = logger;

        }
        public async Task Invoke(IIncomingGrainCallContext context)
        {
            try
            {
                await context.Invoke();
                var msg = string.Format(
                    "{0}.{1}({2}) returned value {3}",
                    context.Grain.GetType(),
                    context.InterfaceMethod.Name,
                    "1",
                    context.Result);
                this.log.Info(msg);
            }
            catch (Exception exception)
            {
                var msg = string.Format(
                    "{0}.{1}({2}) threw an exception: {3}",
                    context.Grain.GetType(),
                    context.InterfaceMethod.Name,
                    "1",
                    exception);
                this.log.Info(msg);

                // If this exception is not re-thrown, it is considered to be
                // handled by this filter.
                throw;
            }
        }
    }
}
