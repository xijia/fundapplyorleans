﻿namespace FundApplyOrleans
{
	partial class ApplicantsApply
	{
		/// <summary>
		/// 必需的设计器变量。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows 窗体设计器生成的代码

		/// <summary>
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			this.label3 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.txtFund = new System.Windows.Forms.TextBox();
			this.richTxtReason = new System.Windows.Forms.RichTextBox();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnUpdate = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.txtState = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.btnEdit = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(63, 26);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(29, 12);
			this.label3.TabIndex = 2;
			this.label3.Text = "Fund";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(63, 69);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(41, 12);
			this.label5.TabIndex = 4;
			this.label5.Text = "Reason";
			// 
			// txtFund
			// 
			this.txtFund.Location = new System.Drawing.Point(159, 23);
			this.txtFund.Name = "txtFund";
			this.txtFund.Size = new System.Drawing.Size(100, 21);
			this.txtFund.TabIndex = 7;
			// 
			// richTxtReason
			// 
			this.richTxtReason.Location = new System.Drawing.Point(159, 66);
			this.richTxtReason.Name = "richTxtReason";
			this.richTxtReason.Size = new System.Drawing.Size(234, 96);
			this.richTxtReason.TabIndex = 9;
			this.richTxtReason.Text = "";
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(65, 256);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 11;
			this.btnCancel.Text = "Cancel(&C)";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnUpdate
			// 
			this.btnUpdate.Location = new System.Drawing.Point(281, 256);
			this.btnUpdate.Name = "btnUpdate";
			this.btnUpdate.Size = new System.Drawing.Size(75, 23);
			this.btnUpdate.TabIndex = 12;
			this.btnUpdate.Text = "Update(&U)";
			this.btnUpdate.UseVisualStyleBackColor = true;
			this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(63, 211);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(65, 12);
			this.label1.TabIndex = 13;
			this.label1.Text = "Your State";
			// 
			// txtState
			// 
			this.txtState.AutoSize = true;
			this.txtState.Location = new System.Drawing.Point(157, 211);
			this.txtState.Name = "txtState";
			this.txtState.Size = new System.Drawing.Size(0, 12);
			this.txtState.TabIndex = 14;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(381, 256);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 17;
			this.button1.Text = "Next(&N)";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// btnEdit
			// 
			this.btnEdit.Location = new System.Drawing.Point(173, 256);
			this.btnEdit.Name = "btnEdit";
			this.btnEdit.Size = new System.Drawing.Size(75, 23);
			this.btnEdit.TabIndex = 20;
			this.btnEdit.Text = "Submit(&S)";
			this.btnEdit.UseVisualStyleBackColor = true;
			this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
			// 
			// ApplicantsApply
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(496, 310);
			this.Controls.Add(this.btnEdit);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.txtState);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btnUpdate);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.richTxtReason);
			this.Controls.Add(this.txtFund);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label3);
			this.Name = "ApplicantsApply";
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox txtFund;
		private System.Windows.Forms.RichTextBox richTxtReason;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnUpdate;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label txtState;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button btnEdit;
	}
}

