﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Runtime;
using GrainInterfaces;

namespace FundApplyOrleans
{
	public partial class Loggin : Form
	{
        private int initial_index = 21;
		private int loggin_index = 21;//安全性太低 人家找到这个不就可以篡改
        //bug 这个是烂的 要改成从0开始 或者一开始获取第一行的index
        IClusterClient client;
        public Loggin(IClusterClient client)
		{
            this.client = client;
            InitializeComponent();
        }

		private void button1_Click(object sender, EventArgs e)
		{
			txtPwd.Text = "";
			txtName.Text = "";
		}

		private async void btnSignUp_Click(object sender, EventArgs e)
		{

            var appl = client.GetGrain<ICommonTools>(0);
            var n = await appl.ApplicantSignUp(txtName.Text, txtPwd.Text);

            switch (n)
            {
                case -2:
                    MessageBox.Show($"user name already exist", "fail",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    break;
                case -1:
                    MessageBox.Show($"bug in sql", "fail",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    break;
                default:
                    MessageBox.Show($"Success Sign Up, Please Log In!", "Success",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                    loggin_index = (initial_index + n);
                            
                    break;
            }
        }

		private async void btnLogin_Click(object sender, EventArgs e)
		{
            var appl = client.GetGrain<ICommonTools>(0);
            var n = await appl.ApplicantSignIn(txtName.Text, txtPwd.Text, true);

            switch (n)
            {
                case null:
                    MessageBox.Show($"user name or password does not exist", "fail",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    break;
                //case -1:
                //    MessageBox.Show($"bug in sql", "fail",
                //    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                //    break;
                default:
                    MessageBox.Show($"Success Log in!", "Success",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                    loggin_index = (int)n;
                    this.Close();
                    var apply = new ApplicantsApply(loggin_index, client);
                    apply.MdiParent = this.MdiParent;
                    apply.Show();
                    break;
            }

        }
    }
}
