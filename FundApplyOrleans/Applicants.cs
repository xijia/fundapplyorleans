﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Runtime;
using GrainInterfaces;
using System.Threading.Tasks;

namespace ApplicantClients
{
    class Applicants
    {
        
    }
}


namespace FundApplyOrleans
{
	static class Applicants
	{
		/// <summary>
		/// 应用程序的主入口点。
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			try
			{
				var client = StartClient().GetAwaiter().GetResult();
				Application.Run(new BigWindow(client));
				client.Close();
			}
			catch (Exception ee)
			{
				MessageBox.Show($"{ee}", "fail",
						   MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
			
		}

		private static async Task<IClusterClient> StartClient()
		{

			IClusterClient client;
			client = new ClientBuilder()
				

				.UseLocalhostClustering()
				.Configure<ClusterOptions>(options =>
				{
					options.ClusterId = "dev";
					options.ServiceId = "FundApply";
				})
				.ConfigureLogging(logging => logging.AddConsole())
				.Build();
			
			await client.Connect();
			return client;
		}
	} 



}
