﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GrainInterfaces;
using Orleans;

namespace FundApplyOrleans
{
	public partial class Account : Form
	{
		private int index;
		private IClusterClient client;
		private Guid guid;

		private ICommonTools commonTools;
		private IApplicantEvents applicantEvents;

		public Account(int insert_id, IClusterClient client, Guid guid)
		{
			this.index = insert_id;
			this.client = client;
			this.guid = guid;
			//需要吗 有点怀疑下面两句
			commonTools = client.GetGrain<ICommonTools>(0);
			applicantEvents = client.GetGrain<IApplicantEvents>(guid);
			InitializeComponent();
			ShowInfo();
		}

		private async void ShowInfo()
		{
			//这里可以改一下 把 basic info 一起传进来 少一次访问
			//var appl = client.GetGrain<ICommonTools>(0);
			var info = await commonTools.FindApplicant(index, "Applicant");
			if (info != null)
			{
				txtAcntNum.Text = info.accountNum;
				txtAccountNumberState.Text = info.AccountNumState;
				txtIsCompleted.Text = info.isCompleted;
			}
			else
			{
				MessageBox.Show($"fail", "fail",
					MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
		}

		private async void btnSubmit_Click(object sender, EventArgs e)
		{

			//var acc = client.GetGrain<IAccountNum>(0);
			//bool response = await acc.ChangeAccountNumState(txtAcntNum.Text,index,1);
			var response = await applicantEvents.FirstPayment(txtAcntNum.Text, index);
			if (response)
			{
				MessageBox.Show($"Subitted!", "Success",
					MessageBoxButtons.OK, MessageBoxIcon.Information);
				ShowInfo();
			}
			else
			{
				MessageBox.Show($"cannot submit please cotact us!", "fail",
					MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
		}

		private async void btnChange_Click(object sender, EventArgs e)
		{
			//var acc = client.GetGrain<IAccountNum>(0);
			//bool response = await acc.ChangeAccountNumState(txtAcntNum.Text, index, 2);
			bool response = await applicantEvents.EditPayment(txtAcntNum.Text, index);
			if (response)
			{
				MessageBox.Show($"Subitted, wait for accountant response!", "Success",
					MessageBoxButtons.OK, MessageBoxIcon.Information);
				ShowInfo();
			}
			else
			{
				MessageBox.Show($"cannot change please cotact us!", "fail",
					MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
		}

		private async void btnAbort_Click(object sender, EventArgs e)
		{
			//我在想automatic trigger的话 这个是不是要单独放成一个event 然后
			//本身abort 就是不做任何操作？
			//var account = client.GetGrain<IAccountNum>(0);
			//var update = await account.ChangeCompleteState(index, 2);
			var update = await applicantEvents.AbortApply(index);
			if (update)
			{
				MessageBox.Show($"updated the state", "success",
				MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				MessageBox.Show($"cannot update the state", "fail",
				MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
		}

		private void btnUpdate_Click(object sender, EventArgs e)
		{
			ShowInfo();
		}
	}
}
