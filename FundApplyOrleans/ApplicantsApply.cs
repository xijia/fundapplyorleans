﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using Orleans;
using GrainInterfaces;

namespace FundApplyOrleans
{
	public partial class ApplicantsApply : Form
	{
        
        private int insert_id = 0;
        private IClusterClient client;
        private Guid guid = Guid.NewGuid();
        public ApplicantsApply(int index, IClusterClient client)
		{
			InitializeComponent();
            insert_id = index;
            //这个竟然不用await吗
            this.client = client;
            ShowInfo(client);

        }
        //client可以最后再关
        public async void ShowInfo(IClusterClient client) 
        {

            var appl = client.GetGrain<ICommonTools>(0);
            var info = await appl.FindApplicant(insert_id,"applicant");
            if (info != null)
            {
                txtFund.Text = info.Fund.ToString();
                richTxtReason.Text = info.Reason;
                //txtAcntNum.Text = info.accountNum;
                txtState.Text = info.States;
            }
            else
            {
                MessageBox.Show($"fail", "fail",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
                   

        }

        //要不要先显示
        private async Task<bool> EditInfo(IClusterClient client, BasicInfo basicInfo)
        {

            var appEvents = client.GetGrain<IApplicantEvents>(guid);
            var res = await appEvents.FillOutApplication(basicInfo, insert_id);
            return res;

            //var appl = client.GetGrain<ICommonTools>(0);
            //bool response = await appl.UpdateInfo(basicInfo, insert_id);
            //return response;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            //txtName.Text = "";
            txtFund.Text = "";
            //txtPwd.Text = "";
            richTxtReason.Text = "";
            //txtAcntNum.Text = "";
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            ShowInfo(client);
        }

        //update button
        private async void btnEdit_Click(object sender, EventArgs e)
        {
            BasicInfo basicInfo = new BasicInfo();
            basicInfo.Fund = int.Parse(txtFund.Text);
            basicInfo.Reason = richTxtReason.Text;
           
            if (await EditInfo(client, basicInfo))
            {
                MessageBox.Show($"Success!", "Success",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show($"fail", "fail",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        //btnNext 忘记改名
        private void button1_Click(object sender, EventArgs e)
        {
            var apply = new Account(insert_id, client, guid);
            apply.MdiParent = this.MdiParent;
            apply.Show();
        }
    }
}
