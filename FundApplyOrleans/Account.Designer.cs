﻿namespace FundApplyOrleans
{
	partial class Account
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtAcntNum = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.btnSubmit = new System.Windows.Forms.Button();
			this.btnAbort = new System.Windows.Forms.Button();
			this.btnChange = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.txtIsCompleted = new System.Windows.Forms.Label();
			this.btnUpdate = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.txtAccountNumberState = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// txtAcntNum
			// 
			this.txtAcntNum.Location = new System.Drawing.Point(154, 104);
			this.txtAcntNum.Name = "txtAcntNum";
			this.txtAcntNum.Size = new System.Drawing.Size(258, 21);
			this.txtAcntNum.TabIndex = 18;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(58, 107);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(89, 12);
			this.label2.TabIndex = 17;
			this.label2.Text = "Account Number";
			// 
			// btnSubmit
			// 
			this.btnSubmit.Location = new System.Drawing.Point(36, 202);
			this.btnSubmit.Name = "btnSubmit";
			this.btnSubmit.Size = new System.Drawing.Size(75, 23);
			this.btnSubmit.TabIndex = 19;
			this.btnSubmit.Text = "Submit(&S)";
			this.btnSubmit.UseVisualStyleBackColor = true;
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			// 
			// btnAbort
			// 
			this.btnAbort.Location = new System.Drawing.Point(337, 202);
			this.btnAbort.Name = "btnAbort";
			this.btnAbort.Size = new System.Drawing.Size(75, 23);
			this.btnAbort.TabIndex = 20;
			this.btnAbort.Text = "Abort";
			this.btnAbort.UseVisualStyleBackColor = true;
			this.btnAbort.Click += new System.EventHandler(this.btnAbort_Click);
			// 
			// btnChange
			// 
			this.btnChange.Location = new System.Drawing.Point(239, 202);
			this.btnChange.Name = "btnChange";
			this.btnChange.Size = new System.Drawing.Size(75, 23);
			this.btnChange.TabIndex = 21;
			this.btnChange.Text = "Change";
			this.btnChange.UseVisualStyleBackColor = true;
			this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(76, 173);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(71, 12);
			this.label1.TabIndex = 22;
			this.label1.Text = "Final State";
			// 
			// txtIsCompleted
			// 
			this.txtIsCompleted.AutoSize = true;
			this.txtIsCompleted.Location = new System.Drawing.Point(160, 173);
			this.txtIsCompleted.Name = "txtIsCompleted";
			this.txtIsCompleted.Size = new System.Drawing.Size(0, 12);
			this.txtIsCompleted.TabIndex = 23;
			// 
			// btnUpdate
			// 
			this.btnUpdate.Location = new System.Drawing.Point(135, 202);
			this.btnUpdate.Name = "btnUpdate";
			this.btnUpdate.Size = new System.Drawing.Size(75, 23);
			this.btnUpdate.TabIndex = 24;
			this.btnUpdate.Text = "Update";
			this.btnUpdate.UseVisualStyleBackColor = true;
			this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(58, 146);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(125, 12);
			this.label3.TabIndex = 25;
			this.label3.Text = "Account Number State";
			// 
			// txtAccountNumberState
			// 
			this.txtAccountNumberState.AutoSize = true;
			this.txtAccountNumberState.Location = new System.Drawing.Point(206, 146);
			this.txtAccountNumberState.Name = "txtAccountNumberState";
			this.txtAccountNumberState.Size = new System.Drawing.Size(0, 12);
			this.txtAccountNumberState.TabIndex = 26;
			// 
			// Account
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(454, 297);
			this.Controls.Add(this.txtAccountNumberState);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.btnUpdate);
			this.Controls.Add(this.txtIsCompleted);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btnChange);
			this.Controls.Add(this.btnAbort);
			this.Controls.Add(this.btnSubmit);
			this.Controls.Add(this.txtAcntNum);
			this.Controls.Add(this.label2);
			this.Name = "Account";
			this.Text = "Account";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtAcntNum;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnSubmit;
		private System.Windows.Forms.Button btnAbort;
		private System.Windows.Forms.Button btnChange;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label txtIsCompleted;
		private System.Windows.Forms.Button btnUpdate;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label txtAccountNumberState;
	}
}