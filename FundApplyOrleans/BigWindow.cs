﻿using Orleans;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace FundApplyOrleans
{
	public partial class BigWindow : Form
	{
		public BigWindow(IClusterClient client)
		{
			InitializeComponent();
			ShowLoggin(client);
		}

		void ShowLoggin(IClusterClient client)
		{
			var reviewerLoggin = new Loggin(client);
			reviewerLoggin.MdiParent = this;
			reviewerLoggin.Show();
		}
	}



}
