﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Orleans;

namespace GrainInterfaces
{
    public enum Arrow
    {
        Condition,
        Response,
        Include,
        Exclude,
        Milestone
    };


    public class Relation
    {
        public readonly string src;
        public readonly string tgt;
        public readonly Arrow rel;

        public Relation(string src, Arrow rel, string tgt)
        {
            this.src = src;
            this.tgt = tgt;
            this.rel = rel;
        }
    }

    public interface IEventProcess : IGrainWithStringKey
    {

        void addEvent(String name);
        void addRelation(String src, Arrow arr, String tgt);
        //wtf? hashset seems could not be generated
        Task<HashSet<string>> enabled();
        void execute(string e);
        Task<Guid> GetGuid();
        Task<bool> isAccepting();
        Task<HashSet<string>> GetExecuted();
        Task<HashSet<string>> GetIncluded();
        Task<HashSet<string>> GetPending();
        Task<Orleans.Streams.IStreamProvider> TestProvider();
    }

}
