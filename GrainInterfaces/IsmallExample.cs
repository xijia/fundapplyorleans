﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Orleans;
namespace GrainInterfaces
{
	public interface IsmallExample : IGrainWithStringKey
	{
		Task FillOutAnExpenseReport();
		Task WithdrawTheExpenseReport();
		Task Approve();
		Task Payout();
	}
}
