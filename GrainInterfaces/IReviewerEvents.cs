﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Orleans;
namespace GrainInterfaces
{
	public interface IReviewerEvents : IGrainWithGuidKey
	{
		Task<bool> Reviewer1Appove(int applicantIndex);
		Task<bool> Reviewer1Reject(int applicantIndex);
		Task<bool> LawyerReviewApprove(int applicantIndex);
		Task<bool> LawyerReviewReject(int applicantIndex);
		Task<bool> ArchitectReviewApprove(int applicantIndex);
		Task<bool> ArchitectReviewReject(int applicantIndex);
		Task<bool> Reviewer3Approve(int applicantIndex);
		Task<bool> Reviewer3Reject(int applicantIndex);
		Task<bool> Reviewer4Approve(int applicantIndex);
		Task<bool> Reviewer4Reject(int applicantIndex);
		Task<bool> BoardMeetingPreApprove(int applicantIndex);
		Task<bool> BoardMeetingPreReject(int applicantIndex);
		Task<bool> BoardMeetingApprove(int applicantIndex);
		Task<bool> BoardMeetingReject(int applicantIndex);
		Task<bool> AccountantApprove(string cardNum, int applicantIndex);
		Task<bool> AccountantReject(string cardNum, int applicantIndex);
		Task<bool> CompleteState(int applicantIndex);
	}

}
