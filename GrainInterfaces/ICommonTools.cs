﻿using Orleans;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GrainInterfaces
{
	public interface ICommonTools: IGrainWithIntegerKey
	{
		Task<Nullable<int>> CheckLoggin(string name, string pwd, bool isApplicant);
		Task<BasicInfo> FindApplicant(int index, string ReviewerName);//, string test
		Task<bool> UpdateInfo(BasicInfo basicInfo, int index);
		Task<int> ApplicantSignUp(string name, string pwd);
		Task<bool> UpdateState(int State, int index);
		Task<(int, int)> FindReview(int index);
		void ForTest();
	}

	public class BasicInfo
	{
		public Guid Key { get; set; }
		public string Name { get; set; }
		public int Fund { get; set; }
		public string Reason { get; set; }
		public string accountNum;
		public string Pwd;
		//public ApproveState States { get; set; }
		public string States;
		public Reviews Reviews;
		public string AccountNumState;
		public string AccountNumChanged;
		public string isCompleted;//0 false 1 true 2abort
	}
	public enum ApproveState { Approve, Reject, PreApprove, BoardReview, BoardMeeting, Unknown };
	public struct Reviews
	{
		public bool LayerReview;
		public bool ArchitectReview;
		public bool Review3;
		public bool Review4;
	}


	
}

