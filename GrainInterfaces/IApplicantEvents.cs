﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Orleans;

namespace GrainInterfaces
{
	//the events should be in the parser or in the graph
	public interface IApplicantEvents : IGrainWithGuidKey
	{
		Task<bool> FillOutApplication(BasicInfo basicInfo, int insert_id);
		Task<bool> FirstPayment(string CardNum, int applicantID);
		Task<bool> EditPayment(string CardNum, int applicantID);
		Task<bool> AbortApply(int applicantID);

	}
}
