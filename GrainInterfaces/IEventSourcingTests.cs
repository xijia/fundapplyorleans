﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Orleans;

namespace GrainInterfaces
{
	public interface IEventSourcingTests : IGrainWithIntegerKey
	{
		Task<string> SendGreetings(string greetings);
	}
}
