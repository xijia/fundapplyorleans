﻿using Orleans;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GrainInterfaces
{
	public interface IAccountNum : IGrainWithIntegerKey
	{
		Task<bool> ChangeAccountNumState(string accountNum, int index, int State);
		Task<bool> ChangeCompleteState(int index, int state);
	}
}
