﻿using Grains;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GrainInterfaces;
using Orleans.Streams;

namespace ASimpleExample
{

	

	public partial class Form1 : Form
	{
		private IClusterClient _client;
		IsmallExample _ismallExample;
		public Form1(IClusterClient client)
		{	
			_client = client;
			_ismallExample = _client.GetGrain<IsmallExample>("first");
			
			InitializeComponent();
		}

		private void FilloutAnExpenseReport_Click(object sender, EventArgs e)
		{
			_ismallExample.FillOutAnExpenseReport();
		}

		private void WithdrawTheExpenseReport_Click(object sender, EventArgs e)
		{
			_ismallExample.WithdrawTheExpenseReport();
		}

		private void Approve_Click(object sender, EventArgs e)
		{
			_ismallExample.Approve();
		}

		//private void Reject_Click(object sender, EventArgs e)
		//{
		//	_ismallExample.Reject();
		//}

		private void PayOut_Click(object sender, EventArgs e)
		{
			_ismallExample.Payout();
		}		
	}
}
