﻿namespace ASimpleExample
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.FilloutAnExpenseReport = new System.Windows.Forms.Button();
			this.WithdrawTheExpenseReport = new System.Windows.Forms.Button();
			this.Approve = new System.Windows.Forms.Button();
			this.PayOut = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(53, 60);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(53, 12);
			this.label1.TabIndex = 0;
			this.label1.Text = "Employee";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(53, 141);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(47, 12);
			this.label2.TabIndex = 1;
			this.label2.Text = "Manager";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(53, 227);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(41, 12);
			this.label3.TabIndex = 2;
			this.label3.Text = "Finans";
			// 
			// FilloutAnExpenseReport
			// 
			this.FilloutAnExpenseReport.Location = new System.Drawing.Point(158, 50);
			this.FilloutAnExpenseReport.Name = "FilloutAnExpenseReport";
			this.FilloutAnExpenseReport.Size = new System.Drawing.Size(109, 41);
			this.FilloutAnExpenseReport.TabIndex = 3;
			this.FilloutAnExpenseReport.Text = "FilloutAnExpenseReport";
			this.FilloutAnExpenseReport.UseVisualStyleBackColor = true;
			this.FilloutAnExpenseReport.Click += new System.EventHandler(this.FilloutAnExpenseReport_Click);
			// 
			// WithdrawTheExpenseReport
			// 
			this.WithdrawTheExpenseReport.Location = new System.Drawing.Point(314, 50);
			this.WithdrawTheExpenseReport.Name = "WithdrawTheExpenseReport";
			this.WithdrawTheExpenseReport.Size = new System.Drawing.Size(123, 43);
			this.WithdrawTheExpenseReport.TabIndex = 4;
			this.WithdrawTheExpenseReport.Text = "WithdrawTheExpenseReport";
			this.WithdrawTheExpenseReport.UseVisualStyleBackColor = true;
			this.WithdrawTheExpenseReport.Click += new System.EventHandler(this.WithdrawTheExpenseReport_Click);
			// 
			// Approve
			// 
			this.Approve.Location = new System.Drawing.Point(172, 141);
			this.Approve.Name = "Approve";
			this.Approve.Size = new System.Drawing.Size(75, 23);
			this.Approve.TabIndex = 5;
			this.Approve.Text = "Approve";
			this.Approve.UseVisualStyleBackColor = true;
			this.Approve.Click += new System.EventHandler(this.Approve_Click);
			// 
			// PayOut
			// 
			this.PayOut.Location = new System.Drawing.Point(172, 227);
			this.PayOut.Name = "PayOut";
			this.PayOut.Size = new System.Drawing.Size(75, 23);
			this.PayOut.TabIndex = 7;
			this.PayOut.Text = "PayOut";
			this.PayOut.UseVisualStyleBackColor = true;
			this.PayOut.Click += new System.EventHandler(this.PayOut_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(478, 304);
			this.Controls.Add(this.PayOut);
			this.Controls.Add(this.Approve);
			this.Controls.Add(this.WithdrawTheExpenseReport);
			this.Controls.Add(this.FilloutAnExpenseReport);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button FilloutAnExpenseReport;
		private System.Windows.Forms.Button WithdrawTheExpenseReport;
		private System.Windows.Forms.Button Approve;
		private System.Windows.Forms.Button PayOut;
	}
}

