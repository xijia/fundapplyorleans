﻿using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASimpleExample
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			
			try
			{
				var client = StartClient().GetAwaiter().GetResult();
				Application.Run(new Form1(client));
				client.Close();
			}
			catch (Exception ee)
			{
				MessageBox.Show($"{ee}", "fail",
						   MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
		}

		private static async Task<IClusterClient> StartClient()
		{


			var client = new ClientBuilder()
				//.AddOutgoingGrainCallFilter<OutGoingCallFilter>()
				.UseLocalhostClustering()
				.Configure<ClusterOptions>(options =>
				{
					options.ClusterId = "dev";
					options.ServiceId = "FundApply";
				})
				.ConfigureLogging(logging => logging.AddConsole())
				.Build();

			await client.Connect();
			return client;
		}
	}
}
