﻿

// This is generated code:

    using System;
  using System.Collections.Generic;
  using System.Text;
  using System.Threading.Tasks;
  using GrainInterfaces;
  using Orleans;
  namespace Grains
  {
  	class smallExample : Orleans.Grain, IsmallExample, IIncomingGrainCallFilter
  	{
  	private bool isFirstTime = true;

	public async Task Invoke(IIncomingGrainCallContext context)
		
	{
		//
			
		if (isFirstTime)
		{
			var streamId = await eventGrain.GetStreamGuid();
			myStream = streamProvider.GetStream<EventMsg>(streamId, myStringKey);
			//No storage provider named "PubSubStore" found loading grain type Orleans.Streams.PubSubRendezvousGrain.
			//should add the memory storage, but it seems a little useless

			await myStream.SubscribeAsync(new StreamObserver(log));
			//possibly GC? We are now using total of 12MB memory

			isFirstTime = false;
		}
				
			
		//log.LogInformation("in Invoke");
		await context.Invoke();
	}
    		/*		public override Task OnActivateAsync()
  				{
  					log.LogInformation("in OnActivateAsync");
  					var streamProvider = GetStreamProvider("ForTest");
  
  					stream = streamProvider.GetStream<EventMsg>(myGuid,
  													this.GetPrimaryKeyString());
  					return base.OnActivateAsync();
  				}*/
  		public Task FillOutAnExpenseReport()
  		{
  			/*			var eventGrain = GrainFactory.GetGrain<IEventProcess>("first");
  
  						var streamId = await eventGrain.GetGuid();
  			*/
  			var streamProvider = GetStreamProvider("ForTest");
  			return Task.CompletedTask;
  		}
  
  		public Task WithdrawTheExpenseReport()
  		{
  			/*			var test = GrainFactory.GetGrain<ICommonTools>(0);
  						test.ForTest();*/
  			return Task.CompletedTask;
  		}
  
  		public Task Approve()
  		{
  			return Task.CompletedTask;
  		}
  		public Task Reject()
  		{
  			return Task.CompletedTask;
  		}
  		public Task Payout()
  		{
  			return Task.CompletedTask;
  		}
  
  	}
  }
